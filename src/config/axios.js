import axios from 'axios'

// Axios Instance
export default axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL
  // withCredentials: true
})
