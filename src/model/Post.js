export class Post {
  constructor (data) {
    this.id = +data.id
    this.title = data.title
    this.userId = +data.userId
    this.desk = data.body
  }
}
