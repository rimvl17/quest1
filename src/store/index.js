import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../config/axios'
import { Post } from '../model/Post'
import { User } from '../model/User'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    search: '',
    users: [],
    posts: []
  },
  getters: {
    search: state => {
      return state.search
    },
    posts: state => {
      return state.posts
    },
    users: state => {
      return state.users
    },
    usersCount: state => {
      return state.users.length
    },
    getUserById: state => id => {
      const user = state.users.find((user) => id === user.id)
      return user || false
    }
  },
  mutations: {
    setSearchStr: (state, str) => {
      state.search = str + ''
    },
    addPost: ({ posts }, post) => {
      posts.push(post)
    },
    addUser: ({ users }, user) => {
      users.push(user)
    },
    setSearch: (state, str) => {
      state.search = str + ''
    }
  },
  actions: {
    getPostsFromApi: async ({ commit }) => {
      try {
        const { data } = await axios.get('posts')
        data.forEach((post) => commit('addPost', new Post(post)))
      } catch (err) {
        console.warn(err)
      }
    },
    getUsersFromApi: async ({
      getters: {
        posts,
        users,
        getUserById
      },
      commit
    }) => {
      for (let i = 0; i < posts.length; i++) {
        if (getUserById(posts[i].userId) === false) {
          try {
            const { data } = await axios.get('users/' + posts[i].userId)
            await commit('addUser', new User(data))
          } catch (err) {
            console.warn(err)
          }
        }
      }
    }
  }
})
